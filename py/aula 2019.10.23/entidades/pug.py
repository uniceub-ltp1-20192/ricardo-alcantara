from entidades.cachorro import Cachorro

class Pug(Cachorro):
  def __init__(self,tipoDeRabo = "enrolado"):
    self._tipoDeRabo = tipoDeRabo

  @property
  def tipoDeRabo(self):
    return self._tipoDeRabo

  @tipoDeRabo.setter
  def tipoDeRabo(self,tipoDeRabo):
    self._tipoDeRabo = tipoDeRabo

  def latir(self):
    print("auuu tshi auu tshi")
