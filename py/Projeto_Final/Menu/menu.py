from Validador.validador import Validador
from Dados.dados import Dados
class Menu:
 
    @staticmethod
    def menuPrincipal():
        print("""
0 - Sair
1 - Consultar
2 - Inserir
3 - Alterar
4 - Deletar
""")
        return Validador.validarOpcaoMenu("[0-4]")
    @staticmethod
    def menuConsultar():
        print("""
0 - Voltar
1 - Consultar por Identificador
2 - Consultar por Propriedade
""")
        return Validador.validarOpcaoMenu("[0-2]")

    @staticmethod
    def iniciarMenu():
        d = Dados()
        opMenu = ""
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()
            if opMenu == "1":
                print("entrou em Consultar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        Menu.menuBuscaPorIdentificador(d)
                    elif opMenu == "2":
                        Menu.menuBuscaPorAtributo(d)
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
               
            elif opMenu == "2":
                print("entrou em Inserir")
            elif opMenu == "3":
                print("entrou em alterar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        print("entrou no consultar por Identificador")
                    elif opMenu == "2":
                        print("entrou no consultar por Propriedade")
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "4":
                print("entrou em deletar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        print("entrou no consultar por Identificador")
                    elif opMenu == "2":
                        print("entrou no consultar por Propriedade")
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "0":
                print("Saindo")
            elif opMenu == "":
                print("")
            else:
                print("Informe uma opcao valida")

    @staticmethod
    def menuBuscaPorIdentificador(d :Dados):
        retorno = d.buscarPorIdentificador(Validador.verificarInteiro())
        print("teste")

    @staticmethod
    def menuBuscaPorAtributo(d):
        print("teste")
