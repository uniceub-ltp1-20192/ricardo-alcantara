#atribui o valor 300 para a constante cebolas
cebolas = 300
#atribui o valor 120 para a constante cebolas_na_caixa
cebolas_na_caixa = 120
#atribui o valor 5 para a constante espaco_caixa
espaco_caixa = 5
#atribui o valor 60 para a constante caixa
caixas = 60
#para a constante cebolas_fora_da_caixa atribui o resultado da operação de subtração
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
#para a constante caixas_vazias atribui o resultado da operação de divisão
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
#para a constante caixas_necessarias atribui o resultado da operação de divisão
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

print("Existem", cebolas_na_caixa, "cebolas encaixotadas")
#apresenta o valor da constante cebolas_na_caixa
print("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
#apresenta o valor o resultado da operação atribuida a cebolas_fora_da_caixa
print("Em cada caixa cabem", espaco_caixa, "cebolas")
#apresenta o valor da constante espaco_caixa
print("Ainda temos,", caixas_vazias, "caixas vazias")
#apresenta o valor o resultado da operação atribuida a caixas_vazias
print("Então precisamos de ", caixas_necessarias, "caixas para empacotar todas as cebolas")
#apresenta o valor o resultado da operação atribuia a caixas_necessarias