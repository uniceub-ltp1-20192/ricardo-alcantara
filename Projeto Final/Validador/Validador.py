import re

class Validador:
    @staticmethod
    def validar(expReg, msgInvalido, msgValido):
        teste = False
        while teste == False:
            valor = input("Informe um valor: ")
            verificarEx = re.match(expReg, valor)
            
            if verificarEx == None:
                print(msgInvalido.format(expReg))
            else:
                print(msgValido.format(valor))
                return valor

    @staticmethod
    def validarValorEntrada(valorAtual,msg):
        novoValor = input(msg)
        if novoValor != None and novoValor != "":
            return novoValor
        return valorAtual

    @staticmethod
    def validarValorInformado(valorAtual,textoMsg):
        novoValor = input(textoMsg)
        if novoValor != None and novoValor != "":
            return novoValor
        return valorAtual

    @staticmethod
    def validarcalibre(pcalibre, msg):
        calibre1 = ".40"
        calibre2 = "380" 
        tst = False
        
        while tst == False:
            novocalibre = input(msg)
            if novocalibre == None or novocalibre == '':
                return pcalibre
            if novocalibre == calibre1 or novocalibre == calibre2:
                return novocalibre               
            else:
                print("""Incorrento! Tente escolher entre esses calibres .40 ou 380.""")

    @staticmethod
    def validarPeso(ppeso, msg):
        tsx = False

        while tsx == False:
            novoPeso = input(msg)
            if novoPeso == None or novoPeso == '':
                return ppeso
            if int(novoPeso) >= int(250) and int(novoPeso) <= int(15000):
                return novoPeso
            else:
                print("Incorreto! O peso da arma deve ser entre 250g e 3000g.")

    @staticmethod
    def validarmunicao(pmunicao, msg):
        test = False
        a = 'expansivo'
        b = 'encamisado'
        c = 'tracante'
        d = 'explosivo'

        while test == False:
            novamunicao = input(msg)
            if novamunicao == None or novamunicao == '':
                return pmunicao
            if novamunicao == a or novamunicao == b or novamunicao == c or novamunicao == d:
                return novamunicao
            else:
                print("""Incorrento! Ecolha uma das municaoes: [expansivo][encamisado][tracante][explosivo]""")

    @staticmethod
    def validarMarca(pmarca, msg):
        tst = False
        z = 'tauros'
        x = 'imbel'
        y = 'glock'
        w = 'beretta'
        v = 'luger'

        while tst == False:
            novaMarca = input(msg)
            if novaMarca == None or novaMarca == '':
                return  pmarca
            if novaMarca == z or novaMarca == x or novaMarca == y or novaMarca == w or novaMarca == v:
                return novaMarca
            else:
                print("Incorrento! Escolha uma das marcas: [tauros][imbel][glock][beretta][luger]")