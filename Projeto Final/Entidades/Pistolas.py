from Entidades.Armas import Armas

class Pistola(Armas):
  def __init__(self,calibre = ".40"):
    super().__init__()
    self._calibre = calibre

  @property
  def calibre(self):
    return self._calibre

  @calibre.setter
  def calibre(self,calibre):
    self._calibre = calibre

  def preparar(self):
    print("Pista Quente!!!")
  
  def __str__(self):
    return """
  ########### Pistola ###########
  Identificador: {}
  Marca: {}
  Tipo de Muniçao: {}
  Peso: {}g
  calibre: {}
  ###############################
  """.format(self.identificador,
    self.marca,
    self.municao,
    self.peso,
    self.calibre)